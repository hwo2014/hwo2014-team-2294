import json
import math

class MyBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.tick = -1
        self.throttle_val = 1.0
        self.turbo = False
        self.prev_pos = None
        self.crashed = False

    def set_car(self, data):
        print "setting car to be %s" % data['color']
        self.car = data['color']
        
    def set_turbo(self, data):
        self.turbo = True
        self.turbo_ticks = data['turboDurationTicks']
    
    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        '''return self.msg("createRace", {"botId": {"name": self.name, "key": self.key},
                                        "trackName": "germany",
                                        "password": "aam",
                                        "carCount": 1
                                    })'''
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        if throttle >= 1.0: throttle = 1.0
        if throttle <= 0.0: throttle = 0.2
        self.throttle_val = throttle
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.throttle(1.0)
        
    def on_game_init(self, data):
        self.track = data['race']['track']['pieces']
        i = 0
        '''
        for piece in self.track:
            print i, piece
            i = i+1
            
        print '*'*80
        '''

    def on_car_positions(self, data):
        pos = None
        for d in data:
            if d['id']['color'] == self.car:
                pos = d
                break
        if self.crashed:
            self.ping()
            return
        if self.tick <= 1:
            self.throttle(1.0)
        else:
            trackpiece = self.track[pos['piecePosition']['pieceIndex']]
            next_trackpiece = self.track[(pos['piecePosition']['pieceIndex']+1)%len(self.track)]
            nn_trackpiece = self.track[(pos['piecePosition']['pieceIndex']+2)%len(self.track)]

            # Cases: straight, curved
            if 'radius' in trackpiece:
                # TODO: Add lane compensation into this
                length = (math.fabs(trackpiece['angle']) * math.pi/180) * trackpiece['radius']
                
                dist_percent = pos['piecePosition']['inPieceDistance'] / length * 100
                    
                throttle_change = 0.05
                if self.prev_pos is not None:
                    if math.fabs(pos['angle']) > math.fabs(self.prev_pos['angle']):
                        # The slip is getting worse, take drastic action
                        throttle_change = 0.15
                if dist_percent <= 10 and math.fabs(pos['angle']) > 5:
                    throttle_change = 0.2
                    
                # Slip control
                before = self.throttle_val
                if math.fabs(pos['angle']) > 10:
                        self.throttle(self.throttle_val-(throttle_change*2))
                elif math.fabs(pos['angle']) > 7:
                        self.throttle(self.throttle_val-throttle_change)
                elif math.fabs(pos['angle']) < 3:
                        self.throttle(self.throttle_val+throttle_change)
                else:
                    self.throttle(self.throttle_val)
                #print self.tick, pos['piecePosition']['pieceIndex'], dist_percent, pos['angle'], before, self.throttle_val
            else:
                length = trackpiece['length']
                if 'radius' not in next_trackpiece:
                    self.throttle(1.0)
                elif 'radius' in next_trackpiece:
                    if 'radius' in nn_trackpiece:
                        self.throttle(self.throttle_val - 0.1)
                    else:
                        if pos['piecePosition']['inPieceDistance'] < (length/2):
                            self.throttle(0.8)
                        else:
                            self.throttle(0.6)
                else: # Why is this here???
                    self.throttle(0.5)
            self.prev_pos = pos
            

    def on_crash(self, data):
        if data['color'] == self.car:
            print "Well... fuuuuck", self.throttle_val
            self.crashed = True
        self.ping()
        
    def on_spawn(self, data):
        if data['color'] == self.car:
            print "Re-spawned!"
            self.crashed = False
            # TODO: Put a throttle here?
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.set_car,
            'turboAvailable': self.set_turbo,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if 'gameTick' in msg: 
                self.tick = msg['gameTick']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()
            